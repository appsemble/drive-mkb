import { bootstrap, FormattedMessage } from '@appsemble/preact';
import { Loader } from '@drive-mkb/preact-components';
import { type DayProps } from '@drive-mkb/types';
import { useCallback, useEffect, useState } from 'preact/hooks';

import { CalendarTop } from './components/CalendarTop/index.js';
import { WeekRow } from './components/WeekRow/index.js';
import './index.css';

bootstrap(({ actions, events, ready, utils }) => {
  // Should be good to use with any type of resources
  // This is why leaving 'any' is ok
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const [data, setData] = useState<any[]>();
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(true);
  const [calendarDate, setCalendarDate] = useState<Date>(new Date());

  useEffect(() => {
    ready();
    setLoading(false);
  }, [ready]);

  useEffect(() => {
    const onData = (newData: unknown[], newError: unknown): void => {
      if (newError) {
        setError(true);
        // @ts-expect-error this is valid.
        setData();
      } else {
        setData(newData);
        setError(false);
      }
    };

    events.on.data(onData);

    return () => {
      events.off.data(onData);
    };
  }, [events]);

  if (error) {
    return (
      <p>
        <FormattedMessage id="error" />
      </p>
    );
  }
  // Should be good to use with any type of resources
  // This is why leaving 'any' is ok
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  async function onFieldClick(resource: any): Promise<void> {
    const result = await actions.onClick(resource);
    events.emit.data(result);
  }

  if (loading) {
    return <Loader />;
  }
  // Number of week should be a parameter probably to make calendar visualization more customizable
  function loadDays(initialNumberOfWeeks = 5): DayProps[][] {
    let numberOfWeeks = initialNumberOfWeeks;
    // The first day of the month is needed to find the Monday before that
    const firstDateOfMonth = new Date(calendarDate);
    firstDateOfMonth.setDate(1);

    // Get current day of week
    const dayOfWeek = firstDateOfMonth.getDay() === 0 ? 7 : firstDateOfMonth.getDay();
    const firstMonday = new Date(
      firstDateOfMonth.setDate(firstDateOfMonth.getDate() - dayOfWeek + 1),
    );
    firstMonday.setHours(0, 0, 0, 0);
    const weeks = [];

    for (let j = 0; j < numberOfWeeks; j += 1) {
      const days: DayProps[] = [];

      for (let i = 0; i < 7; i += 1) {
        const first = new Date(firstMonday);
        const currentFieldDate = new Date(first.setDate(first.getDate() + i + j * 7));

        const resourcesOnDate = data
          ? data.filter((d: { startDate: Date; endDate: Date }) => {
              const startDate = new Date(d.startDate);
              startDate.setHours(0, 0, 0, 0);

              const endDate = new Date(d.endDate);
              endDate.setHours(0, 0, 0, 0);

              return startDate <= currentFieldDate && endDate >= currentFieldDate;
            })
          : [];

        const day: DayProps = {
          date: currentFieldDate,
          onClick: onFieldClick,
          resources: resourcesOnDate.length > 0 ? resourcesOnDate : undefined,
          style:
            currentFieldDate.getMonth() === calendarDate.getMonth()
              ? currentFieldDate.setHours(0, 0, 0, 0) === new Date().setHours(0, 0, 0, 0)
                ? 'today'
                : ''
              : 'different',
        };
        days.push(day);
      }

      weeks.push(days);
      // In some cases the month spans over 6 different weeks rather than 5 or 4
      if (
        weeks.length === 5 &&
        weeks[4][6].date < new Date(calendarDate.getFullYear(), calendarDate.getMonth() + 1, 0)
      ) {
        numberOfWeeks += 1;
      }
    }
    return weeks;
  }

  const incrementMonth = useCallback(
    (increment: number): void => {
      const newDate = new Date(calendarDate);
      newDate.setDate(1);
      newDate.setMonth(newDate.getMonth() + increment);
      setCalendarDate(newDate);
    },
    [calendarDate],
  );
  function handleClick(increment: number): void {
    incrementMonth(increment);
  }
  return (
    <div className="calendar-container">
      <CalendarTop date={calendarDate} onClick={handleClick} utils={utils} />
      <table className="calendar-table">
        <thead className="">
          <tr className="calendar-day-names">
            <th>{utils.formatMessage('Monday')}</th>
            <th>{utils.formatMessage('Tuesday')}</th>
            <th>{utils.formatMessage('Wednesday')}</th>
            <th>{utils.formatMessage('Thursday')}</th>
            <th>{utils.formatMessage('Friday')}</th>
            <th>{utils.formatMessage('Saturday')}</th>
            <th>{utils.formatMessage('Sunday')}</th>
          </tr>
        </thead>

        {loadDays().map((week, i) => (
          <WeekRow days={week} key={i} />
        ))}
      </table>
    </div>
  );
});
