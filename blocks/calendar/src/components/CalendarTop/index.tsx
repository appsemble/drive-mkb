import { type Utils } from '@appsemble/sdk';
import { type VNode } from 'preact';

import { CalendarButton } from '../CalendarButton/CalendarButton.js';

interface CalendarTopProps {
  onClick: (increment: number) => unknown;
  date: Date;
  utils: Utils;
}
type Month =
  // eslint-disable-next-line @typescript-eslint/sort-type-constituents
  | 'January'
  | 'February'
  | 'March'
  | 'April'
  | 'May'
  | 'June'
  | 'July'
  | 'August'
  | 'September'
  | 'October'
  | 'November'
  | 'December';

function getMonthName(month: number): Month {
  const months: Month[] = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

  return months[month % 12];
}
export function CalendarTop({ date, onClick, utils }: CalendarTopProps): VNode {
  return (
    <div className="header-container">
      <CalendarButton handleClick={onClick} type="previous" />
      <span className="calendar-top-header">
        {utils.formatMessage(getMonthName(date.getMonth()))}
      </span>
      <CalendarButton handleClick={onClick} type="next" />
    </div>
  );
}
