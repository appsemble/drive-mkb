import { type DayProps } from '@drive-mkb/types';
import { type VNode } from 'preact';

import { DayCell } from '../DayCell/index.js';

interface WeekRowProps {
  days: DayProps[];
}

export function WeekRow({ days }: WeekRowProps): VNode {
  return (
    <tr>
      {days.map((day, i) => (
        <DayCell
          date={day.date}
          key={i}
          onClick={day.onClick}
          resources={day.resources}
          style={day.style}
        />
      ))}
    </tr>
  );
}
