import { useBlock } from '@appsemble/preact';
import { type IconName } from '@appsemble/sdk';
import { Button } from '@drive-mkb/preact-components';
import classNames from 'classnames';
import { type VNode } from 'preact';

interface CalendarButtonProps {
  handleClick: (increment: number) => unknown;
  type: 'next' | 'previous';
}
export function CalendarButton({ handleClick, type }: CalendarButtonProps): VNode {
  const increment = type === 'previous' ? -1 : 1;
  const {
    utils: { fa },
  } = useBlock();
  return (
    <Button
      className={classNames('button', type)}
      icon={
        type === 'previous' ? (fa('chevron-left') as IconName) : (fa('chevron-right') as IconName)
      }
      onClick={() => handleClick(increment)}
    />
  );
}
