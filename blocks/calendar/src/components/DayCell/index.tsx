import { type DayProps } from '@drive-mkb/types';
import classNames from 'classnames';
import { type VNode } from 'preact';

import { ClickableField } from '../ClickableField/index.js';

export function DayCell({ date, onClick, resources, style }: DayProps): VNode {
  return (
    <td>
      <div className={classNames('date', style)}>{date.toLocaleDateString('nl')}</div>

      {resources ? (
        resources.map((r, i) => <ClickableField key={i} onClick={onClick} resource={r} />)
      ) : (
        <span />
      )}
    </td>
  );
}
