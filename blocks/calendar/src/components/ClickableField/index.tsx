import { type Resource } from '@appsemble/types';
import { type VNode } from 'preact';

interface ClickableProps {
  resource: Resource;
  onClick: (resources: Resource) => unknown;
}

export function ClickableField({ onClick, resource }: ClickableProps): VNode {
  return (
    <div>
      <input
        className="clickable-field"
        onClick={() => onClick(resource)}
        type="button"
        value={String(resource.author) === '' ? 'email missing' : String(resource.author)}
      />
    </div>
  );
}
