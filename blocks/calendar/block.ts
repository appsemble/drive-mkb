declare module '@appsemble/sdk' {
  interface Actions {
    /**
     * Action that gets dispatched when a new filter gets applied.
     *
     * This also gets called during the initial load.
     */
    onClick: never;
  }
  interface EventListeners {
    /**
     * This event can be used to receive incoming data to display.
     */
    data: never;
  }
  interface EventEmitters {
    /**
     * Event that gets emitted once the `onLoad` action has finished.
     *
     * It can be triggered again by sending a `refresh` event.
     */
    data: never;
  }

  interface Messages {
    /**
     * This message is displayed if the data is empty.
     */
    empty: never;

    /**
     * This message is displayed if there was a problem loading the data.
     */
    error: never;

    /**
     * This message is displayed if no data has been loaded yet.
     */
    loading: never;

    Monday: never;
    Tuesday: never;
    Wednesday: never;
    Thursday: never;
    Friday: never;
    Saturday: never;
    Sunday: never;

    January: never;
    February: never;
    March: never;
    April: never;
    May: never;
    June: never;
    July: never;
    August: never;
    September: never;
    October: never;
    November: never;
    December: never;
  }
}
