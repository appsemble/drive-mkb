declare module '@appsemble/sdk' {
  interface Actions {
    /**
     * Action that gets dispatched when the button is clicked.
     */
    onClick: never;
  }

  interface Parameters {
    /**
     * The title of the accordion.
     *
     * It is visible when the accordion is collapsed.
     */
    title: Remapper;

    /**
     * The content of the accordion.
     *
     * It is visible when the accordion is expanded.
     */
    content?: Remapper;
  }
}
