A simple block which can be toggled to show or hide additional details in the form of text.

Originally designed for the 'What to do in Eindhoven' app from Drive-mkb.
