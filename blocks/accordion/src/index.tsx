import { bootstrap } from '@appsemble/preact';
import classNames from 'classnames';
import { useCallback, useEffect, useRef, useState } from 'preact/hooks';

bootstrap(({ actions, data, parameters: { content, title }, ready, utils }) => {
  const [, setOpen] = useState(false);
  const detailsRef = useRef<HTMLDetailsElement>(null);

  const toggle = useCallback(() => {
    setOpen((prevState) => !prevState);
    actions.onClick(data);
  }, []);

  useEffect(() => {
    ready();
    detailsRef.current?.addEventListener('toggle', toggle);

    return () => {
      detailsRef.current?.removeEventListener('toggle', toggle);
    };
  }, [ready, toggle]);

  const titleText = utils.remap(title, data) as string;
  const contentText = utils.remap(content!, data) as string;

  return (
    <details onToggle={toggle} ref={detailsRef}>
      <summary>
        <span>{titleText}</span>
        <span className={classNames('icon', utils.fa('chevron-right'))} />
      </summary>

      <p>{contentText}</p>
    </details>
  );
});
