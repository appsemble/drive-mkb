name: Out-of-office
description: Tracks absense
defaultPage: Request Absence

anchors:
  - &appsemble-version 0.22.10
  - &drive-mkb-version 0.0.3

security:
  default:
    role: User
    policy: everyone
  roles:
    User:
      description: Normal user

resources:
  employee:
    roles: [$public]
    schema:
      type: object
      additionalProperties: false
      required:
        - name
      properties:
        name:
          type: string
          maxLength: 100
        email:
          type: string
          format: email
  absence:
    roles: [$public]
    schema:
      type: object
      additionalProperties: false
      required:
        - startDate
        - endDate
      properties:
        startDate:
          type: string
          format: date
        endDate:
          type: string
          format: date
        reason:
          type: string
          maxLength: 500
        replacement:
          type: string
          maxLength: 80
        replacementNote:
          type: string
          maxLength: 500
        author:
          type: string

theme:
  primaryColor: '#673767'

pages:
  - name: Request Absence
    roles: [User]
    blocks:
      - type: form
        version: *appsemble-version
        actions:
          onSubmit:
            type: resource.create
            resource: absence
            remapBefore:
              object.assign:
                author: { user: name }
            onSuccess:
              type: link
              to: Absence information
          onLoadEmployee:
            type: resource.query
            resource: employee
            remapAfter:
              array.map:
                - object.assign:
                    value: { prop: name }
                    label: { prop: name }
        parameters:
          fields:
            - label: { translate: start }
              name: startDate
              type: date
              icon: calendar-days
              requirements: [required: true]
            - label: { translate: end }
              name: endDate
              type: date
              requirements: [required: true]
              icon: calendar-days
            - label: { translate: reason }
              name: reason
              type: string
              multiline: true
            - label: { translate: replacement }
              name: replacement
              type: enum
              action: onLoadEmployee
            - label: { translate: note for replacement }
              name: replacementNote
              type: string
              multiline: true

  - name: Replacements
    roles: [User]
    blocks:
      - type: data-loader
        version: *appsemble-version
        actions:
          onLoad:
            type: resource.query
            resource: employee
        events:
          listen:
            refresh: refreshEmployees
          emit:
            data: employees
      - type: action-button
        version: *appsemble-version
        actions:
          onClick:
            type: link
            to: New Replacement
        parameters:
          icon: plus
      - type: table
        version: *appsemble-version
        parameters:
          fields:
            - value: { prop: name }
              label: { translate: name }
            - value: { prop: email }
              label: { translate: email }
            - button:
                icon: trash
                color: danger
              alignment: right
              onClick: onRemoveReplacementClick
        events:
          listen:
            data: employees
        actions:
          onRemoveReplacementClick:
            type: dialog
            closable: true
            blocks:
              - type: detail-viewer
                version: *appsemble-version
                parameters:
                  fields:
                    - label: { translate: confirmDelete }
                      value: { prop: title }
              - type: button-list
                version: *appsemble-version
                parameters:
                  buttons:
                    - label: { translate: cancel }
                      color: primary
                      onClick: cancelDialog
                    - label: { translate: deleteEmployee }
                      color: danger
                      onClick: removeItem
                actions:
                  cancelDialog:
                    type: dialog.ok
                  removeItem:
                    type: resource.delete
                    resource: employee
                    onSuccess:
                      type: event
                      event: refreshEmployees
                      onSuccess:
                        type: dialog.ok
  - name: New Replacement
    roles: [User]
    blocks:
      - type: form
        version: *appsemble-version
        actions:
          onSubmit:
            type: resource.create
            resource: employee
            onSuccess:
              type: link
              to: Replacements
        parameters:
          fields:
            - label: { translate: name }
              name: name
              type: string
              requirements:
                - required: true
            - label: { translate: email }
              name: email
              type: string
              format: email
              requirements:
                - required: true

  - name: Whom do I replace
    roles: [User]
    blocks:
      - type: data-loader
        version: *appsemble-version
        actions:
          onLoad:
            type: resource.query
            resource: absence
      - type: filter
        version: *appsemble-version
        parameters:
          highlight: dates
          fields:
            - name: dates
              type: date-range
        events:
          emit:
            filtered: filterResults
            refreshed: refreshResults
        actions:
          onLoad:
            type: resource.query
            resource: absence
            query:
              object.from:
                $filter:
                  string.format:
                    template: '{filter}'
                    values:
                      filter:
                        [
                          { prop: $filter },
                          { string.replace: { dates ge: startDate ge } },
                          { string.replace: { dates le: endDate le } },
                        ]
                      author: { user: name }
            remapAfter:
              - array.map:
                  if:
                    condition: { equals: [{ prop: replacement }, { user: name }] }
                    then:
                      object.from:
                        id: { prop: id }
                        author: { prop: author }
                        startDate: { prop: startDate }
                        endDate: { prop: endDate }
                    else: null
      - type: data-notifier
        version: *appsemble-version
        events:
          listen:
            data: refreshResults
            seed: filterResults
          emit:
            data: toBeReplaced

      - type: table
        version: *appsemble-version
        parameters:
          fields:
            - value: { prop: author }
            - value: { prop: startDate }
              label: { translate: from }
            - value: { prop: endDate }
              label: { translate: to }
        events:
          listen:
            data: toBeReplaced
        actions:
          onClick:
            to: Absence details
            type: link

  - name: Absence details
    parameters:
      - id
    blocks:
      - type: data-loader
        version: *appsemble-version
        actions:
          onLoad:
            type: resource.get
            resource: absence
        events:
          emit:
            data: details
      - type: detail-viewer
        version: *appsemble-version
        parameters:
          fields:
            - value: { prop: author }
              label: { translate: name }
              type: string
            - value: { prop: startDate }
              label: { translate: from }
              type: string
            - value: { prop: endDate }
              label: { translate: to }
              type: string
            - value: { prop: reason }
              label: { translate: reason }
              type: string
            - value: { prop: replacement }
              label: { translate: replacement }
              type: string
            - value: { prop: replacementNote }
              label: { translate: note }
              type: string
        events:
          listen:
            data: details

  - name: Absence information
    roles: [User]
    blocks:
      - type: data-loader
        version: *appsemble-version
        actions:
          onLoad:
            type: resource.query
            resource: absence
        events:
          emit:
            data: absences
      - type: data-loader
        version: *appsemble-version
        actions:
          onLoad:
            type: resource.query
            resource: absence
      - type: filter
        version: *appsemble-version
        parameters:
          highlight: dates
          fields:
            - name: dates
              type: date-range
              label: { translate: select timeframe }
              fromLabel: { translate: from }
              toLabel: { translate: to }
            - name: author
              type: string
              label: { translate: search for a specific colleague }
        events:
          emit:
            filtered: filterResults
            refreshed: refreshResults
        actions:
          onLoad:
            type: resource.query
            resource: absence
            query:
              object.from:
                $filter:
                  string.format:
                    template: '{filter}'
                    values:
                      filter:
                        [
                          { prop: $filter },
                          { string.replace: { dates ge: startDate ge } },
                          { string.replace: { dates le: endDate le } },
                        ]
      - type: data-notifier
        version: *appsemble-version
        events:
          listen:
            data: refreshResults
            seed: filterResults
          emit:
            data: absences
      - type: table
        version: *appsemble-version
        parameters:
          fields:
            - value: { prop: author }
            - value: { prop: startDate }
              label: { translate: from }
            - value: { prop: endDate }
              label: { translate: to }
        events:
          listen:
            data: absences
        actions:
          onClick:
            type: link
            to: Absence details

  - name: Calendar
    roles: [User]
    blocks:
      - type: data-loader
        version: *appsemble-version
        actions:
          onLoad:
            type: resource.query
            resource: absence
        events:
          emit:
            data: absences
      - type: '@drive-mkb/calendar'
        version: *drive-mkb-version
        events:
          listen:
            data: absences
        actions:
          onClick:
            type: link
            to: Absence details
