Discover the hidden gems of Eindhoven with our app. This app is specially designed to help both
residents and tourists find fun activities in the city. Whether you're looking for culinary
experiences or cultural outings, our app has it all.

## Most important features

### In-depth information

Unlike generic platforms operating in many cities, our app offers in-depth information specific to
Eindhoven.

### Easy search

Users can easily search by category to see what there is to do in the city.

### Limited publications

To prevent the app from becoming a dumping ground for events, entrepreneurs can only publish 3 to 4
times a month.

### Free for everyone

The app remains free for both consumers and entrepreneurs to attract and keep a broad audience.

### No immediate login required

Consumers do not have to log in immediately, but can do so later, for example to subscribe to the
newsletter.

Our app is more than just a guide; it is a platform that brings the local community together and
celebrates the unique culture of Eindhoven. Discover what Eindhoven has to offer today!

Of course, Eindhoven is just an example and the app is a basis for launching in any city.
