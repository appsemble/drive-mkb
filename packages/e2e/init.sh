#!/bin/bash

nohup bash -c '
  export PGPASSWORD=password
  until psql -h localhost -p 5432 -U admin -d testAppsemble -c "\dt" >/dev/null 2>&1
  do
    echo "Waiting for tables to be created..."
    sleep 5
  done
  psql -h localhost -p 5432 -U admin -d testAppsemble -f /init.sql
' &
