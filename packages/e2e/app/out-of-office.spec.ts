import { type ElementHandle, expect } from '@playwright/test';

import { test } from '../fixtures/test/index.js';

const findEmployeeInRows = async (
  tableRows: ElementHandle<HTMLElement | SVGElement>[],
  name: string,
  email: string,
): Promise<ElementHandle<HTMLElement | SVGElement> | null> => {
  for (const tableRow of tableRows) {
    const cells = await tableRow.$$('td');
    const rowName = await cells[0].textContent();
    const rowEmail = await cells[1].textContent();
    if (rowName === name && rowEmail === email) {
      return cells[2].$('div > button');
    }
  }
  return null;
};

test.describe('Out-of-office', () => {
  test.beforeEach(async ({ appsemble }) => {
    appsemble.setApp('out-of-office');
    await appsemble.studioLogin();
    await appsemble.visitApp();
    await appsemble.loginApp();
  });

  test.describe.serial('Employee flow', () => {
    const mockEmployeeName = String(Date.now());
    const mockEmployeeEmail = `${mockEmployeeName}@example.com`;

    test('should create a new employee', async ({ page }) => {
      await page.click('[href="/en/new-replacement"]');

      await page.waitForSelector('.appsemble-loader', { state: 'hidden' });
      await expect(page.getByRole('heading', { name: 'New Replacement' })).toBeVisible();

      await page.fill('input[id="name"]', mockEmployeeName);
      await page.fill('input[id="email"]', mockEmployeeEmail);
      await page.click('button[type="submit"]');

      await page.waitForSelector('.appsemble-loader', { state: 'hidden' });
      await expect(page.getByRole('heading', { name: 'Replacements' })).toBeVisible();

      await page.waitForSelector('table tbody', { state: 'visible' });

      const updatedRows = await page.$$('tbody tr');

      const newEmployee = await findEmployeeInRows(
        updatedRows,
        mockEmployeeName,
        mockEmployeeEmail,
      );

      expect(newEmployee).not.toBe(null);
    });

    test('should delete employee', async ({ page }) => {
      await page.click('[href="/en/replacements"]');

      await page.waitForSelector('.appsemble-loader', { state: 'hidden' });
      await expect(page.getByRole('heading', { name: 'Replacements' })).toBeVisible();

      const rows = await page.$$('tbody tr');

      const deleteMockUserButton = await findEmployeeInRows(
        rows,
        mockEmployeeName,
        mockEmployeeEmail,
      );
      await deleteMockUserButton?.click();
      await page.click('.is-centered > button.is-danger');

      // This part is a bit inconsistent
      // Need to wait for the page to load the updated content
      // eslint-disable-next-line playwright/no-networkidle
      await page.waitForLoadState('networkidle');

      const updatedRows = await page.$$('table tbody tr');

      expect(await findEmployeeInRows(updatedRows, mockEmployeeName, mockEmployeeEmail)).toBeNull();
    });
  });

  test.describe.serial('Create absence flow', () => {
    const date = new Date();
    const options: Intl.DateTimeFormatOptions = {
      weekday: 'long',
      month: 'long',
      day: 'numeric',
      year: 'numeric',
    };
    const name = 'Bot';
    const startDate = new Date(date);
    startDate.setDate(date.getDate() + 1);
    const formattedStartDate = startDate.toLocaleDateString('en-US', options);

    const endDate = new Date(date);
    endDate.setDate(date.getDate() + 2);
    const formattedEndDate = endDate.toLocaleDateString('en-US', options);

    const absenceCreated = async (
      tableRows: ElementHandle<HTMLElement | SVGElement>[],
    ): Promise<boolean> => {
      for (const tableRow of tableRows) {
        const cells = await tableRow.$$('td');
        const nameText = await cells[0].textContent();
        const startDateText = await cells[1].textContent();
        const endDateText = await cells[2].textContent();

        if (
          nameText === name &&
          startDateText === startDate.toISOString().split('T')[0] &&
          endDateText === endDate.toISOString().split('T')[0]
        ) {
          return true;
        }
      }
      return false;
    };

    test('should create a new absence', async ({ page }) => {
      await page.click('[href="/en/replacements"]');

      await page.waitForSelector('.appsemble-loader', { state: 'hidden' });
      await expect(page.getByRole('heading', { name: 'Replacements' })).toBeVisible();

      const rows = await page.$$('table tbody tr');

      if (!(await findEmployeeInRows(rows, 'Bot', 'bot@appsemble.com'))) {
        await page.click('[href="/en/new-replacement"]');

        await page.waitForSelector('.appsemble-loader', { state: 'hidden' });
        await expect(page.getByRole('heading', { name: 'New Replacement' })).toBeVisible();

        await page.fill('input[id="name"]', 'Bot');
        await page.fill('input[id="email"]', 'bot@appsemble.com');
        await page.click('button[type="submit"]');
        await page.waitForLoadState('load');
      }

      await page.click('[href="/en/request-absence"]');

      // Need to wait for the options on the replacement field
      // To be fetched, otherwise test will fail
      // eslint-disable-next-line playwright/no-networkidle
      await page.waitForLoadState('networkidle');

      await page.waitForSelector('.appsemble-loader', { state: 'hidden' });

      await page.click('input[id="startDate"]');
      await page.click(`span[aria-label="${formattedStartDate}"]`);

      await page.click('input[id="endDate"]');
      const elements = await page.$$(`span[aria-label="${formattedEndDate}"]`);
      await elements[1].click();

      await page.fill('textarea[id="reason"]', String(Date.now()));
      await page.fill('textarea[id="replacementNote"]', String(Date.now()));

      await page.selectOption('select[id="replacement"]', { label: 'Bot' });
      await page.click('button[type="submit"]');

      await page.waitForLoadState('domcontentloaded');
      await page.waitForSelector('.appsemble-loader', { state: 'hidden' });

      await expect(page.getByRole('heading', { name: 'Absence information' })).toBeVisible();
      await page.waitForSelector('table');

      // Check if absence was created successfully - if is in the table
      const rowContents = await page.$$('tbody tr');

      const created = await absenceCreated(rowContents);
      expect(created).toBe(true);
    });

    // The absence created in the first test
    // Should be visible on both pages
    test("should find the created absence in the 'Whom do I replace' page", async ({ page }) => {
      await page.click('[href="/en/whom-do-i-replace"]');

      await page.waitForSelector('.appsemble-loader', { state: 'hidden' });
      await expect(page.getByRole('heading', { name: 'Whom do I replace' })).toBeVisible();

      const rowContents = await page.$$('tbody tr');

      const created = await absenceCreated(rowContents);
      expect(created).toBe(true);
    });
  });
});
