import { expect } from '@playwright/test';

import { test } from '../fixtures/test/index.js';

test.setTimeout(120_000);

test.describe('What to do in Eindhoven', () => {
  test.beforeEach(async ({ appsemble }) => {
    appsemble.setApp('eindhoven-to-do');
    await appsemble.visitApp();
  });

  test.describe.serial('Main flow', () => {
    const business = String(Date.now());
    const email = `${business}@email.com`;
    const password = 'password';

    test('Request to join', async ({ page }) => {
      await page.waitForSelector('.navbar-item > a');
      await page.click('.navbar-item > a');

      await page.waitForSelector('a[href="/en/request-to-join"]');
      await page.click('a[href="/en/request-to-join"]');

      await page.waitForSelector('#displayName');
      await page.fill('input[name="displayName"]', business);
      await page.waitForSelector('#email');
      await page.fill('input[name="email"]', email);
      await page.waitForSelector('#password');
      await page.fill('input[name="password"]', password);
      await page.click('button[type="submit"]');

      await page.waitForURL('**/request-sent');
      await page.waitForSelector('div[data-path-index="pages.9.blocks.1"]');
      const paragraph = await page.$('p[data-content="paragraph"]');
      const paragraphText = await paragraph?.innerText();

      await page.screenshot({
        path: 'test-results/screenshots/eindhoven-to-do/request-to-join.png',
      });

      expect(paragraphText).toEqual(
        'Thank you for your request to participate. When your request has been approved, you will receive an email.',
      );
    });

    test('Create new event', async ({ baseURL, page }) => {
      await page.waitForSelector('.navbar-item > a');
      await page.click('.navbar-item > a');

      await page.waitForSelector('#email');
      await page.fill('input[name="email"]', email);
      await page.waitForSelector('#password');
      await page.fill('input[name="password"]', password);

      const submit = await page.$('button[type="submit"]');

      await page.waitForSelector('button[type="submit"]');

      await submit?.click();

      await page.waitForURL('**/events-manager');
      const baseUrl = new URL(String(baseURL));

      if (process.env.NODE_ENV === 'development') {
        baseUrl.protocol = 'http:';
        baseUrl.host = 'localhost:9999';
      }

      await page.goto(
        `${baseUrl.protocol}//eindhoven-to-do.drive-mkb.${baseUrl.host}/en/create-event`,
      );

      const name = 'Test Event';

      await page.screenshot({ path: 'test-results/screenshots/eindhoven-to-do/waiting.png' });
      await page.waitForSelector('#name');
      await page.fill('#name', name);
      await page.fill('#description', 'Test description');
      await page.fill('#price', '0');
      await page.fill('#location', 'Test Location');

      await page.focus('input[type="file"]');
      await page.setInputFiles(
        'input[type="file"]',
        'test-results/screenshots/eindhoven-to-do/waiting.png',
      );
      await page.click('input#datetime');

      const today = new Date();
      today.setDate(today.getDate() + 1);
      const daysOfWeek = [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
      ];
      const months = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
      ];

      await page.click(
        `span[aria-label="${daysOfWeek[today.getDay()]}, ${
          months[today.getMonth()]
        } ${today.getDate()}, ${today.getFullYear()} 12:00 AM"] + span`,
      );
      await page.click('form[data-path="pages.create-event.blocks.2"]');

      await page.fill('#externalLink', 'https://www.example.com');
      await page.selectOption('#ageCategory', '3');

      await page.click('button[data-field="tags"]');
      await page.waitForSelector('form[name="tags"]');
      await page.check('input#Design-events');
      await page.check('input#Festivals');
      await page.click('button[data-field="tags"]');
      await page.check('label[for="outdoorActivity"]');

      await page.click('button[type="submit"]');
      await page.waitForURL('**/events-manager');

      await page.waitForSelector('table');
      const table = await page.$('table');
      const tableText = await table?.innerText();

      expect(tableText).toContain(name);

      await page.screenshot({ path: 'test-results/screenshots/eindhoven-to-do/create-event.png' });
    });

    test('Edit event', async ({ page }) => {
      page.setDefaultTimeout(90_000);
      await page.waitForSelector('.navbar-item > a');
      await page.click('.navbar-item > a');

      await page.waitForSelector('#email');
      await page.fill('input[name="email"]', email);
      await page.waitForSelector('#password');
      await page.fill('input[name="password"]', password);

      const submit = await page.$('button[type="submit"]');

      await page.waitForSelector('button[type="submit"]');

      await submit?.click();
      await page.waitForURL('**/events-manager');

      await page.waitForSelector('button[aria-haspopup="true"]');
      const button = await page.$('button[aria-haspopup="true"]');
      await button?.click();
      await page.waitForSelector('td div[role="menu"] .dropdown-content');

      const editButton = await page.$('td div[role="menu"] button:first-of-type');
      await editButton?.click();

      await page.waitForURL('**/edit-event');
      await page.waitForSelector('h1');

      // Date should be later than now

      const options: Intl.DateTimeFormatOptions = {
        weekday: 'long',
        month: 'long',
        day: 'numeric',
        year: 'numeric',
      };

      const newDate = new Date();
      newDate.setDate(newDate.getDate() + 1);
      const formattedDate = newDate.toLocaleDateString('en-US', options);

      await page.click('input[id="datetime"]');
      await page.click(`span[aria-label*="${formattedDate}"]`);

      const name = 'Test Event';
      await page.waitForSelector('#name');
      await page.fill('#name', `${name} Edited`);

      // If there is already a uploaded, remove before adding the new one
      const btnRemoveFile = await page.$('button[class*="button is-small"]');

      if (btnRemoveFile) {
        await btnRemoveFile.click();
      }

      await page.focus('input[type="file"]');
      await page.setInputFiles(
        'input[type="file"]',
        'test-results/screenshots/eindhoven-to-do/waiting.png',
      );

      await page.click('button[type="submit"]');
      await page.waitForURL('**/events-manager');
      await page.waitForSelector('table');
      const table = await page.$('table');
      const tableText = await table?.innerText();

      expect(tableText).toContain(`${name} Edited`);

      await page.screenshot({ path: 'test-results/screenshots/eindhoven-to-do/edit-event.png' });
    });

    test('Delete event', async ({ page }) => {
      await page.waitForSelector('.navbar-item > a');
      await page.click('.navbar-item > a');

      await page.waitForSelector('#email');
      await page.fill('input[name="email"]', email);
      await page.waitForSelector('#password');
      await page.fill('input[name="password"]', password);

      const submit = await page.$('button[type="submit"]');

      await page.waitForSelector('button[type="submit"]');

      await submit?.click();
      await page.waitForURL('**/events-manager');

      await page.waitForSelector('button[aria-haspopup="true"]');
      const button = await page.$('button[aria-haspopup="true"]');
      await button?.click();
      await page.waitForSelector('td div[role="menu"] .dropdown-content');

      const existingTableRows = await page.$$('table tbody tr');
      const existingTableRowCount = existingTableRows.length;

      const deleteButton = await page.$('td div[role="menu"] button:last-of-type');
      await deleteButton?.click();

      await page.waitForSelector(
        'div[data-path-index="pages.5.blocks.3.actions.deleteEvent.blocks.1"]',
      );
      const modal = await page.$('.modal-card');
      const modalCloseButton = await modal?.$('button.delete');

      const confirmButtonShadowHost = await modal?.$(
        'div[data-path-index="pages.5.blocks.3.actions.deleteEvent.blocks.1"] > div',
      );

      const hasShadowRoot = await page.evaluateHandle(
        (element) => Boolean(element?.shadowRoot),
        confirmButtonShadowHost,
      );

      if (await hasShadowRoot.jsonValue()) {
        const shadowRootHandle = await confirmButtonShadowHost?.evaluateHandle(
          (element) => element.shadowRoot,
        );

        const confirmButton = await shadowRootHandle?.asElement()?.$('button.button');

        if (confirmButton) {
          await confirmButton.click();
        } else {
          // Handle the case when the element is not a shadow host
          await modal?.click();
          await modalCloseButton?.focus();
          await modalCloseButton?.press('Tab');

          await page.keyboard.press('Enter');
        }

        // Continue with your actions on the element inside the shadow root
      } else {
        // Handle the case when the element is not a shadow host
        await modalCloseButton?.focus();
        await modalCloseButton?.press('Tab');

        await page.keyboard.press('Enter');
      }

      await page.waitForTimeout(3000);
      await page.reload();
      await page.waitForLoadState('domcontentloaded');
      const remainingTableRows = await page.$$('table tbody tr');
      const remainingTableRowCount = remainingTableRows.length;

      expect(remainingTableRowCount).toBeLessThan(existingTableRowCount);

      await page.screenshot({ path: 'test-results/screenshots/eindhoven-to-do/delete-event.png' });
    });
  });
});
