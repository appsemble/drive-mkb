import { type Resource } from '@appsemble/types';

export interface DayProps {
  date: Date;
  onClick: (resources: Resource | Resource[]) => unknown;
  resources?: Resource[];
  style: string;
}
