stages:
  - test
  - publish
  - e2e

workflow:
  rules:
    # By default, run jobs for every merge request.
    - if: $CI_MERGE_REQUEST_ID
    # Don't run the pipeline twice if it was triggered from the main Appsemble repository
    - if: $CI_MERGE_REQUEST_ID && $CI_PIPELINE_SOURCE == 'pipeline'
      when: never
    # By default, run jobs for every commit on main.
    - if: $CI_COMMIT_BRANCH == 'main'
    # By default, run jobs for every schedule
    - if: $CI_PIPELINE_SOURCE == 'schedule'

###################################################################################################
#  Job Templates                                                                                  #
###################################################################################################
default:
  image: node:18.18.0-bookworm-slim
  before_script:
    - npm ci

# A preset for running Docker in Docker.
.docker:
  interruptible: true
  services:
    - docker:dind
  image: docker
  dependencies: []
  before_script:
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
###################################################################################################
# Test Stage                                                                                      #
###################################################################################################

# Lint code using ESLint
eslint:
  stage: test
  script: npx eslint --format gitlab .
  artifacts:
    reports:
      codequality: gl-codequality.json

# Check formatting using Prettier
prettier:
  stage: test
  script: npx prettier --check .

# Validate the app translations
translations:
  stage: test
  script: npx appsemble app extract-messages --verify en apps/*

e2e:
  interruptible: true
  stage: e2e
  extends: .docker
  needs: []
  variables:
    APPSEMBLE_IMAGE: '${APPSEMBLE_IMAGE:-appsemble/appsemble}'
  script:
    - docker compose --file docker-compose-e2e.yaml up -d
    - chmod +x scripts/*
    - . scripts/e2e.sh
  after_script:
    - docker compose --file docker-compose-e2e.yaml down --volumes
  artifacts:
    expose_as: Playwright videos
    when: always
    paths:
      - packages/e2e/test-results/
    reports:
      junit: packages/e2e/junit.xml

####################################################################################################
## Publish stage                                                                                  ##
####################################################################################################

# Publish apps to staging
staging:
  stage: publish
  environment:
    name: staging
    url: https://staging.appsemble.review/organizations/drive-mkb
  rules:
    - if: $CI_PIPELINE_SOURCE == 'schedule'
  script:
    - npx appsemble organization upsert drive-mkb --name 'Drive MKB' --remote
      https://staging.appsemble.review
    - npx appsemble app publish --context staging --resources --modify-context apps/*

# Publish apps to production
production:
  stage: publish
  environment:
    name: production
    url: https://appsemble.app/organizations/drive-mkb
  rules:
    - if: $CI_COMMIT_REF_NAME == 'main' && $CI_PIPELINE_SOURCE != 'schedule'
  script:
    - npx appsemble config set remote https://appsemble.app/
    - npx appsemble block publish --ignore-conflict blocks/*
    - npx appsemble app update --resources --assets --force --context production apps/*
    - npx appsemble app update --resources --assets --force --context demo
      apps/what-to-do-in-eindhoven
